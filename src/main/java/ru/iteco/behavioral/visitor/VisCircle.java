package ru.iteco.behavioral.visitor;

/**
 * VisLongCactus.
 *
 * @author Ilya_Sukhachev
 */
public class VisCircle extends VisShape {

    public VisCircle(int sideA, int sideB) {
        super(sideA, sideB);
        setRadius((int) (Math.sqrt(sideA * sideA + sideB * sideB) / 2));
    }
}
