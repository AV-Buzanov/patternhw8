package ru.iteco.behavioral.visitor;

import ru.iteco.behavioral.visitor.*;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {
    public static void main(String[] args) {

        VisShape[] homeplants2 = {
                new VisCircle(200, 300),
                new VisRectangle(200, 300),
                new VisTriangle(200, 300)};

        var shapeVisitor = new ShapeVisitor();
        for (VisShape plant : homeplants2) {
            plant.draw(shapeVisitor, 200, 500);
            System.out.println(plant.getPerimetr(shapeVisitor));
            System.out.println(plant.getArea(shapeVisitor));
        }

    }
}
