package ru.iteco.behavioral.visitor;

/**
 * Visitor.
 *
 * @author Ilya_Sukhachev
 */
public abstract class Visitor {

    public abstract void draw(VisShape shape, int x, int y);
    public abstract double getArea(VisShape shape);
    public abstract double getPerimetr(VisShape shape);

}
