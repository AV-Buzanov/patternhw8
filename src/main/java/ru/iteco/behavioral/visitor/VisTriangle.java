package ru.iteco.behavioral.visitor;

/**
 * VisLongCactus.
 *
 * @author Ilya_Sukhachev
 */
public class VisTriangle extends VisShape {

    public VisTriangle(int sideA, int sideB) {
        super(sideA, sideB);
    }
}
