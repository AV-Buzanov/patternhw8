package ru.iteco.behavioral.visitor;

/**
 * VisLongCactus.
 *
 * @author Ilya_Sukhachev
 */
public class VisRectangle extends VisShape {

    public VisRectangle(int sideA, int sideB) {
        super(sideA, sideB);
    }
}
