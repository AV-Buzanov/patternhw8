package ru.iteco.behavioral.visitor;

import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * CareVisitor.
 *
 * @author Ilya_Sukhachev
 */
public class ShapeVisitor extends Visitor {

    @Override
    public void draw(VisShape shape, int x, int y) {
        Window window = new Window(new Frame());
        if (shape instanceof VisRectangle) {
            window.setShape(new Rectangle(shape.getSideA(), shape.getSideB()));
        }
        if (shape instanceof VisCircle) {
            window.setShape(new Ellipse2D.Double(0, 0, shape.getSideA(), shape.getSideB()));
        }
        if (shape instanceof VisTriangle) {
            int[] xPoints = new int[3];
            xPoints[0] = shape.getSideA();
            int[] yPoints = new int[3];
            yPoints[2] = shape.getSideB();
            Polygon polygon = new Polygon(xPoints, yPoints, 3);
            window.setShape(polygon);
        }
        window.setSize(shape.getSideA(), shape.getSideB());
        window.setAlwaysOnTop(true);
        window.setBackground(Color.RED);
        window.setLocation(x, y);
        window.show();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        window.dispose();
    }

    @Override
    public double getArea(VisShape shape) {
        if (shape instanceof VisRectangle) {
            return shape.getSideB() * shape.getSideA();
        }
        if (shape instanceof VisCircle) {
            return Math.PI * Math.multiplyExact(shape.getRadius(), shape.getRadius());
        }
        if (shape instanceof VisTriangle) {
            return (shape.getSideB() * shape.getSideA()) / 2;
        }
        return 0;
    }

    @Override
    public double getPerimetr(VisShape shape) {
        if (shape instanceof VisRectangle) {
            return Math.multiplyExact(2, shape.getSideB() + shape.getSideA());
        }
        if (shape instanceof VisCircle) {
            return 2 * Math.PI * shape.getRadius();
        }
        if (shape instanceof VisTriangle) {
            return Math.sqrt(shape.getSideA() * shape.getSideA() + shape.getSideB() * shape.getSideB()) + shape.getSideA() + shape.getSideB();
        }
        return 0;
    }
}
