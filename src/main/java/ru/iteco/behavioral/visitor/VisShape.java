package ru.iteco.behavioral.visitor;

/**
 * VisHomeplants.
 *
 * @author Ilya_Sukhachev
 */
public abstract class VisShape {

    public int sideA;

    public int sideB;

    public int radius;

    public VisShape(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public VisShape(int radius) {
        this.radius = radius;
    }

    public void draw(Visitor visitor, int x, int y) {
        visitor.draw(this, x, y);
    }

    public double getArea(Visitor visitor) {
        return visitor.getArea(this);
    }

    public double getPerimetr(Visitor visitor) {
        return visitor.getPerimetr(this);
    }

    public int getSideA() {
        return sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
